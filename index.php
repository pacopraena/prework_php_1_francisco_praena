<?php
function weekDay($number){
    switch($number){
        case 1:
            echo 'Lunes';
            break;
        case 2:
            echo 'Martes';
            break;
        case 3:
            echo 'Miércoles';
            break;
        case 4:
            echo 'Jueves';
            break;
        case 5:
            echo 'Viernes';
            break;
        case 6:
            echo 'Sábado';
            break;
        case 7:
            echo 'Domingo';
            break;
        default:
            echo 'Ese número no corresponde a un día de la semana';
            break;
    }
}

/*
weekDay(5);
echo '<br/>';
weekDay(8);
echo '<br/>';
weekDay(3);
echo '<br/>';
weekDay(0);
echo '<br/>';
*/